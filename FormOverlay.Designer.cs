﻿namespace WinFormsBrowserOverlay
{
    partial class FormOverlay
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.winFormsBrowserView1 = new DotNetBrowser.WinForms.WinFormsBrowserView();
            this.SuspendLayout();
            // 
            // winFormsBrowserView1
            // 
            this.winFormsBrowserView1.AcceptLanguage = null;
            this.winFormsBrowserView1.AudioMuted = null;
            this.winFormsBrowserView1.BrowserType = DotNetBrowser.BrowserType.HEAVYWEIGHT;
            this.winFormsBrowserView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.winFormsBrowserView1.InitialFocusOwner = false;
            this.winFormsBrowserView1.Location = new System.Drawing.Point(0, 0);
            this.winFormsBrowserView1.Name = "winFormsBrowserView1";
            this.winFormsBrowserView1.Preferences = null;
            this.winFormsBrowserView1.Size = new System.Drawing.Size(600, 600);
            this.winFormsBrowserView1.TabIndex = 0;
            this.winFormsBrowserView1.URL = null;
            this.winFormsBrowserView1.ZoomLevel = null;
            // 
            // FormOverlay
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(600, 600);
            this.Controls.Add(this.winFormsBrowserView1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormOverlay";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private DotNetBrowser.WinForms.WinFormsBrowserView winFormsBrowserView1;
    }
}

